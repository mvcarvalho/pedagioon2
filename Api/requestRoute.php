<?php
if (isset($_GET["mylat"]) && isset($_GET["mylon"]) && isset($_GET["destlat"]) && isset($_GET["destlon"])){
    if(!checkStartEndLatLng()){
        checkStartEndCities();
    }
}else{
    echo '{"return":"false"}';
}

function checkStartEndLatLng(){
    $sSQL = "SELECT * FROM `routes_cache` WHERE "
        . $_GET["destlat"] ." > (`endlat` - 0.05) AND " . $_GET["destlat"] ." < (`endlat` + 0.05)  AND "
        . $_GET["destlon"] ." > (`endlng` - 0.05) AND " . $_GET["destlon"] ." < (`endlng` + 0.05) AND "
        . $_GET["mylat"] ." > (`startlat` - 0.05) AND " . $_GET["mylat"] ." < (`startlat` + 0.05) AND "
        . $_GET["mylon"] ." > (`startlng` - 0.05) AND " . $_GET["mylon"] ." < (`startlng` + 0.05)";

    $conn = new mysqli("srv62.prodns.com.br","pedagioo_dba","(!Ggv2CsWF!a","pedagioo_system");
    //$conn = new mysqli("localhost","root","","pedagioon");
    $query = $conn->query($sSQL);
    if($row = $query->fetch_array()){
        $conn->close();
        echo $row["json"];
        return true;
    }else{
        $conn->close();
        return false;
    }
}

function checkStartEndCities(){
    //Verifica a cidade da latitude e longitude de origem informados para em seguida requisitar as informações gerais da cidade.
    $request = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $_GET["mylat"] . "," . $_GET["mylon"] . "&sensor=false");
    $json = json_decode($request, true);
    $cidade = $json['results'][1]['formatted_address'];
	$mycity = $cidade;
	
    $cidade = urlencode($cidade);
    $request = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $cidade . "&sensor=false");
    $json = json_decode($request, true);
    $mylat = $json['results'][0]['geometry']['location']['lat'];
    $mylon = $json['results'][0]['geometry']['location']['lng'];


    //Verifica a cidade da latitude e longitude de destino informados para em seguida requisitar as informações gerais da cidade.
    $request = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $_GET["destlat"] . "," . $_GET["destlon"] . "&sensor=false");
    $json = json_decode($request, true);
	
	$cidade = $json['results'][1]['formatted_address'];
	$destcity = $cidade;
	
    $cidade = urlencode($cidade);
    $request = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . $cidade . "&sensor=false");
    $json = json_decode($request, true);
    $destlat = $json['results'][0]['geometry']['location']['lat'];
    $destlon = $json['results'][0]['geometry']['location']['lng'];

    //Verifica se existe cache para as cidades informadas
    $sSQL = "SELECT * FROM `routes_cache` WHERE startcity = ´" . $mycity . "´ AND endcity = ´" . $destcity . "´";
    $conn = new mysqli("srv62.prodns.com.br","pedagioo_dba","(!Ggv2CsWF!a","pedagioo_system");
    //$conn = new mysqli("localhost","root","","pedagioon");
    if($query = $conn->query($sSQL)){
        if($row = $query->fetch_array()){
            $conn->close();
            echo "cidades <br><br>";
            echo $row["json"];
            return true;
        }else{
            $conn->close();
            echo getRouteFromGoogle($mylat, $mylon, $destlat, $destlon, $mycity, $destcity);
        }
    }else{
        $conn->close();
        echo getRouteFromGoogle($mylat, $mylon, $destlat, $destlon, $mycity, $destcity);
    }
}

function getRouteFromGoogle($mylat, $mylon, $destlat, $destlon, $mycity, $destcity){
    $request = file_get_contents("http://maps.googleapis.com/maps/api/directions/json?origin=" . $mylat . "," . $mylon . "&destination=" . $destlat . "," . $destlon . "&sensor=false");
    $conn = new mysqli("srv62.prodns.com.br","pedagioo_dba","(!Ggv2CsWF!a","pedagioo_system");
    //$conn = new mysqli("localhost","root","","pedagioon");
    $sSQL = "INSERT INTO routes_cache(`id`, `startlat`, `endlat`, `startlng`, `endlng`, `startcity`, `endcity`, `json`) VALUES (null,?,?,?,?,?,?,?)";
    $query = $conn->prepare($sSQL);
    $query->bind_param("ddddsss", $mylat, $destlat, $mylon, $destlon, $mycity, $destcity, $request);
    $query->execute();
    $conn->close();

    return $request;
}
?>