<?php
if (isset($_GET["mylat"]) && isset($_GET["mylon"]) && isset($_GET["destlat"]) && isset($_GET["destlon"])){
    if(!checkCloserTolls()){
        echo '{"return":"false"}';
    }
}else{
    echo '{"return":"false"}';
}

function checkCloserTolls(){
    $sSQL = "SELECT * FROM `routes_cache` WHERE "
        . $_GET["destlat"] ." > (`endlat` - 0.05) AND " . $_GET["destlat"] ." < (`endlat` + 0.05)  AND "
        . $_GET["destlon"] ." > (`endlng` - 0.05) AND " . $_GET["destlon"] ." < (`endlng` + 0.05) AND "
        . $_GET["mylat"] ." > (`startlat` - 0.05) AND " . $_GET["mylat"] ." < (`startlat` + 0.05) AND "
        . $_GET["mylon"] ." > (`startlng` - 0.05) AND " . $_GET["mylon"] ." < (`startlng` + 0.05)";

    $conn = new mysqli("srv62.prodns.com.br","pedagioo_dba","(!Ggv2CsWF!a","pedagioo_system");
    //$conn = new mysqli("localhost","root","","pedagioon");
    $query = $conn->query($sSQL);
    if($row = $query->fetch_array()){

        $sSQL = "SELECT * FROM  `tolls`";
        $query2 = $conn->query($sSQL);
        $tolls = array();
        while($toll = $query2->fetch_array(MYSQL_ASSOC)){
            $tolls[] = $toll;
        };
        $conn->close();

        $closerTolls = array();
        $json = json_decode($row["json"], true);
        foreach($json['routes'][0]['legs'][0]['steps'] as $step){
            $array = decodePolylineToArray($step['polyline']['points']);
            foreach($array as $a){
                foreach($tolls as $toll){
                    if( checkCloserPoint($a[0],$a[1],$toll['latitude'],$toll['longitude'])){
                        if(!in_array($toll, $closerTolls)){
                            $closerTolls[] = $toll;
                        }
                    }
                }
            }
        }

        $rtn = '{ "tolls" : [ ';
        foreach($closerTolls as $toll){
            $rtn = $rtn . '{ "id":' . $toll['id'];
            $rtn = $rtn . ', "highwayname":"' . $toll['highwayname'] . '"';
            $rtn = $rtn . ', "highway":"' . $toll['highway'] . '"';
            $rtn = $rtn . ', "km":' . $toll['km'];
            $rtn = $rtn . ', "latitude":' . $toll['latitude'];
            $rtn = $rtn . ', "longitude":' . $toll['longitude'];
			$rtn = $rtn . ', "pricecar":' . $toll['pricecar'];
			$rtn = $rtn . ', "pricebike":' . $toll['pricebike'];
            $rtn = $rtn . ', "priceaxle":' . $toll['priceaxle'] . '},';
        }
        $rtn = substr($rtn,0,-1) . ']}';

        echo $rtn;
        return true;
    }else{
        $conn->close();
        return false;
    }
}

function checkCloserPoint($lat1, $lon1, $lat2, $lon2){
    if( ($lat1 > ($lat2 - 0.005)) &&
        ($lat1 < ($lat2 + 0.005)) &&
        ($lon1 > ($lon2 - 0.005)) &&
        ($lon1 < ($lon2 + 0.005))){
        return true;
    }else{
        return false;
    }
}

function decodePolylineToArray($encoded)
{
    $length = strlen($encoded);
    $index = 0;
    $points = array();
    $lat = 0;
    $lng = 0;

    while ($index < $length)
    {
        $b = 0;

        $shift = 0;
        $result = 0;
        do{
            $b = ord(substr($encoded, $index++)) - 63;

            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        }while ($b >= 0x20);

        $dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));

        $lat += $dlat;

        $shift = 0;
        $result = 0;
        do
        {
            $b = ord(substr($encoded, $index++)) - 63;
            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        }
        while ($b >= 0x20);

        $dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
        $lng += $dlng;

        $points[] = array($lat * 1e-5, $lng * 1e-5);
    }

    return $points;
}
?>