package br.com.pedagioon.pedagioon2.vos;

import java.util.ArrayList;
import java.util.List;

import br.com.pedagioon.pedagioon2.factories.MarkerOptionsFactory;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class TravelPointVO {

	private static long idSeed = 0;

	public static List<LatLng> getLatLngList(final ArrayList<TravelPointVO> pointsVOMap) {
		final ArrayList<LatLng> rtn = new ArrayList<LatLng>();

		for (final TravelPointVO travelPointVO : pointsVOMap) {
			rtn.add(travelPointVO.getLatLng());
		}

		return rtn;
	}

	private final int icon;
	private final long id = TravelPointVO.idSeed++;

	private final LatLng latLng;

	private MarkerOptions markerOptions;

	public TravelPointVO(final double lat, final double lng, final int resId) {
		this(new LatLng(lat, lng), resId);
	}

	public TravelPointVO(final LatLng latLng, final int resId) {
		this.latLng = latLng;
		this.icon = resId;
	}

	public int getIcon() {
		return this.icon;
	}

	public long getId() {
		return this.id;
	}

	public LatLng getLatLng() {
		return this.latLng;
	}

	public MarkerOptions getMarkerOptions() {
		if (this.markerOptions == null) {
			this.markerOptions = MarkerOptionsFactory.get(this.icon);
			this.markerOptions.position(this.latLng);
		}

		return this.markerOptions;
	}

}
