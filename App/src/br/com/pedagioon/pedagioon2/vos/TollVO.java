package br.com.pedagioon.pedagioon2.vos;

import java.io.Serializable;

public class TollVO implements Serializable {

    private static final long serialVersionUID = -1492716977226607721L;
    private int               id;
    private int               km;
    private double            latitude;
    private double            longitude;
    private String            nomeRodovia;
    private double            priceaxle;
    private double            pricebike;
    private double            pricecar;
    private String            siglaRodovia;

    public int getId() {
        return this.id;
    }

    public int getKm() {
        return this.km;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String getNomeRodovia() {
        return this.nomeRodovia;
    }

    public double getPriceaxle() {
        return this.priceaxle;
    }

    public double getPricebike() {
        return this.pricebike;
    }

    public double getPricecar() {
        return this.pricecar;
    }

    public String getSiglaRodovia() {
        return this.siglaRodovia;
    }

    public void setId( final int id ) {
        this.id = id;
    }

    public void setKm( final int km ) {
        this.km = km;
    }

    public void setLatitude( final double latitude ) {
        this.latitude = latitude;
    }

    public void setLongitude( final double longitude ) {
        this.longitude = longitude;
    }

    public void setNomeRodovia( final String nomeRodovia ) {
        this.nomeRodovia = nomeRodovia;
    }

    public void setPriceaxle( final double priceaxle ) {
        this.priceaxle = priceaxle;
    }

    public void setPricebike( final double pricebike ) {
        this.pricebike = pricebike;
    }

    public void setPricecar( final double pricecar ) {
        this.pricecar = pricecar;
    }

    public void setSiglaRodovia( final String siglaRodovia ) {
        this.siglaRodovia = siglaRodovia;
    }
}
