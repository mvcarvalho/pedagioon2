package br.com.pedagioon.pedagioon2.vos;

import java.io.Serializable;
import java.util.ArrayList;

public class RouteVO implements Serializable {
	private static final long	serialVersionUID	= 6389379869563346443L;
	private double				distance;
	private double				endLat;
	private double				endLng;
	private String				points;
	private double				startLat;
	private double				startLng;
	private ArrayList<TollVO>	tolls;

	public double getDistance() {
		return this.distance;
	}

	public double getEndLat() {
		return this.endLat;
	}

	public double getEndLng() {
		return this.endLng;
	}

	public String getPoints() {
		return this.points;
	}

	public double getStartLat() {
		return this.startLat;
	}

	public double getStartLng() {
		return this.startLng;
	}

	public ArrayList<TollVO> getTolls() {
		return this.tolls;
	}

	public void setDistance(final double d) {
		this.distance = d;
	}

	public void setEndLat(final double endLat) {
		this.endLat = endLat;
	}

	public void setEndLng(final double endLng) {
		this.endLng = endLng;
	}

	public void setPoints(final String points) {
		this.points = points;
	}

	public void setStartLat(final double startLat) {
		this.startLat = startLat;
	}

	public void setStartLng(final double startLng) {
		this.startLng = startLng;
	}

	public void setTolls(final ArrayList<TollVO> tolls) {
		this.tolls = tolls;
	}

}
