package br.com.pedagioon.pedagioon2.utils;

import android.content.res.Resources;

public class DPUtil {

    public static int dpToPx( final int dp ) {
        return ( int ) ( dp * Resources.getSystem().getDisplayMetrics().density );
    }

    public static int pxToDp( final int px ) {
        return ( int ) ( px / Resources.getSystem().getDisplayMetrics().density );
    }

}
