package br.com.pedagioon.pedagioon2.utils;

import com.google.android.gms.maps.model.LatLng;

public class LatLngUtil {

    public static String getGoogleAPIFormat( final LatLng latLng ) {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append( latLng.latitude );
        stringBuilder.append( ',' );
        stringBuilder.append( latLng.longitude );

        return stringBuilder.toString();
    }

}
