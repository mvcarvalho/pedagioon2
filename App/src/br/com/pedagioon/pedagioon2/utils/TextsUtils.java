package br.com.pedagioon.pedagioon2.utils;

public class TextsUtils {

	public static String formatDistences(final int pKm1, final int pKm2) {
		return String.valueOf(pKm1) + "km / " + String.valueOf(pKm2) + "km";
	}
}
