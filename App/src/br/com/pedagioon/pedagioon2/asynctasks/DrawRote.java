package br.com.pedagioon.pedagioon2.asynctasks;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import br.com.pedagioon.pedagioon2.interfaces.RouteSource;
import br.com.pedagioon.pedagioon2.utils.DPUtil;
import br.com.pedagioon.pedagioon2.utils.LatLngUtil;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

public class DrawRote extends AsyncTask<String, String, String> {
	private ProgressDialog		pDialog;
	private final RouteSource	routeSource;

	public DrawRote(final RouteSource routeSource) {
		this.routeSource = routeSource;
	}

	@Override
	protected String doInBackground(final String... args) {
		final LatLng startLatLng = this.routeSource.routeSourceGetStartLocation();
		final LatLng endLatLng = this.routeSource.routeSourceGetEndLocation();
		String startLocation = LatLngUtil.getGoogleAPIFormat(startLatLng);
		String endLocation = LatLngUtil.getGoogleAPIFormat(endLatLng);
		startLocation = startLocation.replace(" ", "+");
		endLocation = endLocation.replace(" ", "+");
		;
		final String stringUrl = "http://maps.googleapis.com/maps/api/directions/json?origin=" + startLocation + "&destination=" + endLocation + "&sensor=false";
		final StringBuilder response = new StringBuilder();
		try {
			final URL url = new URL(stringUrl);
			final HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
			if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				final BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
				String strLine = null;

				while ((strLine = input.readLine()) != null) {
					response.append(strLine);
				}
				input.close();
			}

			final String jsonOutput = response.toString();
			final JSONObject jsonObject = new JSONObject(jsonOutput);

			// routesArray contains ALL routes
			final JSONArray routesArray = jsonObject.getJSONArray("routes");
			// Grab the first route
			final JSONObject route = routesArray.getJSONObject(0);

			final JSONObject poly = route.getJSONObject("overview_polyline");
			final String polyline = poly.getString("points");

			this.routeSource.routeSourceSetPolyz(this.routeSource.routeSourceDecodePoly(polyline));

		} catch (final Exception e) {
			// Toast.makeText( this.routeSource.routeSourceGetContext(),
			// "Erro!!", Toast.LENGTH_LONG ).show();
		}

		return null;

	}

	@Override
	protected void onPostExecute(final String file_url) {

		if ((this.routeSource.routeSourceGetPolyz() != null) && (this.routeSource.routeSourceGetPolyz().size() > 0)) {
			for (int i = 0; i < (this.routeSource.routeSourceGetPolyz().size() - 1); i++) {
				final LatLng src = this.routeSource.routeSourceGetPolyz().get(i);
				final LatLng dest = this.routeSource.routeSourceGetPolyz().get(i + 1);
				this.routeSource.routeSourceGetMap().addPolyline(new PolylineOptions().add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude)).width(DPUtil.dpToPx(5)).color(Color.BLUE).geodesic(true));
			}
		}

		this.pDialog.dismiss();
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		this.pDialog = new ProgressDialog(this.routeSource.routeSourceGetContext());
		this.pDialog.setMessage("Preparando rota");
		this.pDialog.setIndeterminate(false);
		this.pDialog.setCancelable(false);
		this.pDialog.show();
	}

}