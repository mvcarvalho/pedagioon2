package br.com.pedagioon.pedagioon2.asynctasks;

import org.json.JSONObject;

import android.os.AsyncTask;
import br.com.mlweb.libs.network.Requests;
import br.com.pedagioon.pedagioon2.interfaces.IWebRequestListener;

public class WebRequestAsyncTask extends AsyncTask<String, String, String> {

	public static final int REQUEST_ROUTE = 10001;
	public static final int REQUEST_TOLLS = 10002;

	private static final String SERVER_ADDRESS = "http://www.pedagioon.com.br/api/";
	private static final String WEB_SERVICE_ROUTE = "route.php";
	private static final String WEB_SERVICE_TOLLS = "tolls.php";

	private final IWebRequestListener mIWebRequestListener;
	private int mRequest = 0;

	public WebRequestAsyncTask(final IWebRequestListener pIWebRequestListener) {
		this.mIWebRequestListener = pIWebRequestListener;
	}

	@Override
	protected String doInBackground(final String... params) {
		String result = null;

		if (params.length < 4) {
			this.mIWebRequestListener.onResultError("Informe todos os parāmetros corretamente.", this.mRequest);

		} else {
			String url = WebRequestAsyncTask.SERVER_ADDRESS;

			if (this.mRequest == WebRequestAsyncTask.REQUEST_ROUTE) {
				url = url + WebRequestAsyncTask.WEB_SERVICE_ROUTE + "?";

			} else if (this.mRequest == WebRequestAsyncTask.REQUEST_TOLLS) {
				url = url + WebRequestAsyncTask.WEB_SERVICE_TOLLS + "?";
			}

			url = url + "mylat=" + params[0];
			url = url + "&mylon=" + params[1];
			url = url + "&destlat=" + params[2];
			url = url + "&destlon=" + params[3];

			result = Requests.requestGet(url);
		}
		return result;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(final String result) {
		JSONObject jsonObject = null;

		try {
			jsonObject = new JSONObject(result);

			if (jsonObject.has("erro")) {
				this.mIWebRequestListener.onResultError(jsonObject.getString("erro"), this.mRequest);
			} else {
				this.mIWebRequestListener.onResultOk(jsonObject.toString(), this.mRequest);
			}
		} catch (final Exception e) {
			this.mIWebRequestListener.onResultError("Erro ao receber dados do servidor.", this.mRequest);
			e.printStackTrace();
		}
	}

	public void setRequest(final int pRequest) {
		this.mRequest = pRequest;
	}
}
