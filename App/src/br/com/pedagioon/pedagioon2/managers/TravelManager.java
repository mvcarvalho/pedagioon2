package br.com.pedagioon.pedagioon2.managers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import br.com.mlweb.libs.messages.ToastUtils;
import br.com.pedagioon.pedagioon2.asynctasks.WebRequestAsyncTask;
import br.com.pedagioon.pedagioon2.factories.MarkerOptionsFactory;
import br.com.pedagioon.pedagioon2.interfaces.IRouteListener;
import br.com.pedagioon.pedagioon2.interfaces.IWebRequestListener;
import br.com.pedagioon.pedagioon2.interfaces.RouteSource;
import br.com.pedagioon.pedagioon2.utils.DPUtil;
import br.com.pedagioon.pedagioon2.vos.RouteVO;
import br.com.pedagioon.pedagioon2.vos.TollVO;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

public class TravelManager implements RouteSource, IWebRequestListener {

	private static TravelManager	INSTANCE;

	private IRouteListener			mIRouteListener;
	private ProgressDialog			pDialog;
	private LatLng					mStartLocation;
	private LatLng					mEndLocation;
	private RouteVO					route;
	private List<LatLng>			mPolyz;
	private Context					mContext;
	private boolean					isGps;
	private boolean					isEnabled;

	public static TravelManager getInstance() {
		if (TravelManager.INSTANCE == null) {
			TravelManager.INSTANCE = new TravelManager();
			TravelManager.INSTANCE.route = new RouteVO();
			TravelManager.INSTANCE.isGps = false;
			TravelManager.INSTANCE.isEnabled = false;
		}
		return TravelManager.INSTANCE;
	}

	public void setIRouteListener(final IRouteListener pIRouteListener) {
		this.mIRouteListener = pIRouteListener;
	}

	public void setContext(final Context pContext) {
		this.mContext = pContext;
	}

	public void setGps(final boolean isGps) {
		this.isGps = isGps;
	}

	public boolean isEnabled() {
		return this.isEnabled;
	}

	public RouteVO getRoute() {
		return this.route;
	}

	public void setStartLocation(final LatLng pLatLng) {
		this.mStartLocation = pLatLng;
		this.getRoute().setStartLat(pLatLng.latitude);
		this.getRoute().setStartLng(pLatLng.longitude);
	}

	public void setEndLocation(final LatLng pLatLng) {
		this.mEndLocation = pLatLng;
		this.getRoute().setEndLat(pLatLng.latitude);
		this.getRoute().setEndLng(pLatLng.longitude);
	}

	public boolean isLocationsSetted() {
		if ((this.mStartLocation != null) && (this.mEndLocation != null)) {
			return true;
		}
		return false;
	}

	private void showLoadDialog(final String pMessage) {
		this.pDialog = new ProgressDialog(this.mContext);
		this.pDialog.setMessage(pMessage);
		this.pDialog.setIndeterminate(false);
		this.pDialog.setCancelable(false);
		this.pDialog.show();
	}

	public void calculateRoute() {

		this.showLoadDialog("Calculando rota");

		this.mIRouteListener.getGoogleMap().clear();

		final WebRequestAsyncTask webRequestAsyncTask = new WebRequestAsyncTask(this);
		webRequestAsyncTask.setRequest(WebRequestAsyncTask.REQUEST_ROUTE);

		final String[] latLngs = { String.valueOf(this.mStartLocation.latitude), String.valueOf(this.mStartLocation.longitude), String.valueOf(this.mEndLocation.latitude), String.valueOf(this.mEndLocation.longitude) };
		webRequestAsyncTask.execute(latLngs[0], latLngs[1], latLngs[2], latLngs[3]);
	}

	public void calculateTolls() {
		final WebRequestAsyncTask webRequestAsyncTask = new WebRequestAsyncTask(this);
		webRequestAsyncTask.setRequest(WebRequestAsyncTask.REQUEST_TOLLS);

		final String[] latLngs = { String.valueOf(this.mStartLocation.latitude), String.valueOf(this.mStartLocation.longitude), String.valueOf(this.mEndLocation.latitude), String.valueOf(this.mEndLocation.longitude) };
		webRequestAsyncTask.execute(latLngs[0], latLngs[1], latLngs[2], latLngs[3]);
	}

	@Override
	public List<LatLng> routeSourceDecodePoly(final String encoded) {
		final List<LatLng> poly = new ArrayList<LatLng>();
		int index = 0;
		final int len = encoded.length();
		int lat = 0, lng = 0;

		while (index < len) {
			int b, shift = 0, result = 0;

			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			final int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));

			lat += dlat;
			shift = 0;
			result = 0;

			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			final int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));

			lng += dlng;

			final LatLng p = new LatLng(((lat / 1E5)), ((lng / 1E5)));

			poly.add(p);
		}

		return poly;
	}

	@Override
	public Context routeSourceGetContext() {
		return this.mIRouteListener.getContext();
	}

	@Override
	public LatLng routeSourceGetEndLocation() {
		return this.mEndLocation;
	}

	@Override
	public GoogleMap routeSourceGetMap() {
		return this.mIRouteListener.getGoogleMap();
	}

	@Override
	public List<LatLng> routeSourceGetPolyz() {
		return this.mPolyz;
	}

	@Override
	public LatLng routeSourceGetStartLocation() {
		return this.mStartLocation;
	}

	@Override
	public void routeSourceSetPolyz(final List<LatLng> decodePoly) {
		this.mPolyz = decodePoly;
	}

	@Override
	public void onResultOk(final String pResult, final int pRequest) {
		JSONObject jsonObject;
		if (pRequest == WebRequestAsyncTask.REQUEST_ROUTE) {
			try {
				jsonObject = new JSONObject(pResult);
				final String polyline = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONObject("overview_polyline").getString("points");
				this.getRoute().setDistance(jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getDouble("value") / 1000);
				this.mPolyz = this.routeSourceDecodePoly(polyline);
			} catch (final JSONException e) {
				e.printStackTrace();
			}

			if ((this.mPolyz != null) && (this.mPolyz.size() > 0)) {
				if (!this.isGps) {
					for (int i = 0; i < (this.mPolyz.size() - 1); i++) {
						final LatLng src = this.mPolyz.get(i);
						final LatLng dest = this.mPolyz.get(i + 1);
						this.mIRouteListener.getGoogleMap().addPolyline(new PolylineOptions().add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude, dest.longitude)).width(DPUtil.dpToPx(5)).color(Color.BLUE).geodesic(true));
					}
				}

				LatLng latLng = this.mPolyz.get(0);
				this.mIRouteListener.getGoogleMap().addMarker(MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TYPE_GREEN_LITTLE).position(latLng));
				this.setStartLocation(latLng);

				latLng = this.mPolyz.get(this.mPolyz.size() - 1);
				this.mIRouteListener.getGoogleMap().addMarker(MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TYPE_RED_LITTLE).position(latLng));
				this.setEndLocation(latLng);

				this.calculateTolls();
			}
		} else if (pRequest == WebRequestAsyncTask.REQUEST_TOLLS) {
			try {
				jsonObject = new JSONObject(pResult);
				final ArrayList<TollVO> tollVOs = new ArrayList<TollVO>();

				if (jsonObject.has("notolls")) {
					ToastUtils.showCenteredToast(this.mIRouteListener.getContext(), "Esta rota n�o possui ped�gios.");

				} else {
					final JSONArray jsonArray = jsonObject.getJSONArray("tolls");

					for (int x = 0; x < jsonArray.length(); x++) {
						final JSONObject jsonObject2 = jsonArray.getJSONObject(x);

						final TollVO tollVO = new TollVO();
						tollVO.setId(jsonObject2.getInt("id"));
						tollVO.setKm(jsonObject2.getInt("km"));
						tollVO.setLatitude(jsonObject2.getDouble("latitude"));
						tollVO.setLongitude(jsonObject2.getDouble("longitude"));
						tollVO.setNomeRodovia(jsonObject2.getString("highway"));
						tollVO.setSiglaRodovia(jsonObject2.getString("highwayname"));
						tollVO.setPriceaxle(jsonObject2.getDouble("priceaxle"));
						tollVO.setPricecar(jsonObject2.getDouble("pricecar"));
						tollVO.setPricebike(jsonObject2.getDouble("pricebike"));

						tollVOs.add(tollVO);
					}
				}

				this.getRoute().setTolls(tollVOs);
			} catch (final JSONException e) {
				e.printStackTrace();
			}

			for (final TollVO tollVO : this.getRoute().getTolls()) {
				final LatLng latLng = new LatLng(tollVO.getLatitude(), tollVO.getLongitude());
				this.mIRouteListener.getGoogleMap().addMarker(MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TOLL).position(latLng));
			}

			this.isEnabled = true;
			this.pDialog.dismiss();
		}
	}

	@Override
	public void onResultError(final String pResult, final int pRequest) {
		ToastUtils.showCenteredToast(this.mIRouteListener.getContext(), "Erro: " + pResult);
		this.pDialog.dismiss();
	}
}
