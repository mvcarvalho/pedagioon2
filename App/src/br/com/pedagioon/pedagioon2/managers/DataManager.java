package br.com.pedagioon.pedagioon2.managers;

import android.content.Context;
import android.content.SharedPreferences;

public class DataManager {

	private static DataManager INSTANCE;

	private static final String SYS_PREFERENCES = "DataManager.SYS_PREFERENCES";
	private static final String CONSUMPTION = "DataManager.CONSUMPTION";
	private static final String FUEL_PRICE = "DataManager.FUEL_PRICE";
	private static final String AXLES_NUMBER = "DataManager.AXLES_NUMBER";

	private SharedPreferences mSharedPreferences;
	private SharedPreferences.Editor mEditor;

	private float mConsumption;
	private float mFuelPrice;
	private int mAxlesNumber;

	public static DataManager getInstance() {
		if (DataManager.INSTANCE == null) {
			DataManager.INSTANCE = new DataManager();
		}
		return DataManager.INSTANCE;
	}

	public void loadData(Context pContext) {
		if (this.mSharedPreferences == null) {
			this.mSharedPreferences = pContext.getSharedPreferences(DataManager.SYS_PREFERENCES, Context.MODE_PRIVATE);
		}

		this.mConsumption = this.mSharedPreferences.getFloat(DataManager.CONSUMPTION, 10);
		this.mFuelPrice = this.mSharedPreferences.getFloat(DataManager.FUEL_PRICE, 2);
		this.mAxlesNumber = this.mSharedPreferences.getInt(DataManager.AXLES_NUMBER, 2);
	}

	public void saveData(Context pContext) {
		if (this.mSharedPreferences == null) {
			this.mSharedPreferences = pContext.getSharedPreferences(DataManager.SYS_PREFERENCES, Context.MODE_PRIVATE);
		}

		if (this.mEditor == null) {
			this.mEditor = this.mSharedPreferences.edit();
		}

		this.mEditor.putFloat(DataManager.CONSUMPTION, this.mConsumption);
		this.mEditor.putFloat(DataManager.FUEL_PRICE, this.mFuelPrice);
		this.mEditor.putInt(DataManager.AXLES_NUMBER, this.mAxlesNumber);

		this.mEditor.commit();
	}

	public float getConsumption() {
		return this.mConsumption;
	}

	public void setConsumption(float pConsumption) {
		this.mConsumption = pConsumption;
	}

	public float getFuelPrice() {
		return this.mFuelPrice;
	}

	public void setFuelPrice(float pFuelPrice) {
		this.mFuelPrice = pFuelPrice;
	}

	public int getAxlesNumber() {
		return this.mAxlesNumber;
	}

	public void setAxlesNumber(int pAxlesNumber) {
		this.mAxlesNumber = pAxlesNumber;
	}

}
