package br.com.pedagioon.pedagioon2.interfaces;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;

public interface IRouteListener {
	public GoogleMap getGoogleMap();

	public Context getContext();
}
