package br.com.pedagioon.pedagioon2.interfaces;

public interface IWebRequestListener {
	public void onResultOk(String pResult, int pRequest);

	public void onResultError(String pResult, int pRequest);
}
