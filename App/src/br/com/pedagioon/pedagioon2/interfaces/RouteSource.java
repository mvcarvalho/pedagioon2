package br.com.pedagioon.pedagioon2.interfaces;

import java.util.List;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public interface RouteSource {

    List<LatLng> routeSourceDecodePoly( String polyline );

    Context routeSourceGetContext();

    LatLng routeSourceGetEndLocation();

    GoogleMap routeSourceGetMap();

    List<LatLng> routeSourceGetPolyz();

    LatLng routeSourceGetStartLocation();

    void routeSourceSetPolyz( List<LatLng> decodePoly );

}
