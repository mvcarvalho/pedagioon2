package br.com.pedagioon.pedagioon2.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.mlweb.libs.GpsTracker;
import br.com.mlweb.libs.ILocationListener;
import br.com.mlweb.libs.animation.Animation;
import br.com.mlweb.libs.messages.ToastUtils;
import br.com.mlweb.libs.network.Connectivity;
import br.com.pedagioon.pedagioon2.R;
import br.com.pedagioon.pedagioon2.factories.MarkerOptionsFactory;
import br.com.pedagioon.pedagioon2.interfaces.IRouteListener;
import br.com.pedagioon.pedagioon2.managers.TravelManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements ILocationListener, OnMapLongClickListener, IRouteListener {

	public static final String	NEW_TRAVEL		= "MapActivity.NEW_TRAVEL";
	public static final String	GPS				= "MapActivity.GPS";

	public static final int		GPS_SETTINGS	= 10001;
	public static final int		REQUEST_INFO	= 10002;

	private SupportMapFragment	mGoogleMapFragment;
	private GpsTracker			mGpsTracker;
	private MarkerOptions		mMarker;

	private float				mZoom			= 15;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.loadData();
		this.initComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_exit:
			this.finish();
			Animation.setTransitionLeftRight(this);
			break;

		case R.id.action_travel:
			if (TravelManager.getInstance().isEnabled()) {
				this.startActivityForResult(new Intent(this, TravelActivity.class), MapActivity.REQUEST_INFO);
				Animation.setTransitionBottonUp(this);
			} else {
				ToastUtils.showCenteredToast(this, "Primeiro voc� precisa montar uma viagem. Selecione os pontos de origem e destino para isso.");
			}
			break;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onBackPressed() {
		if (this.mGpsTracker != null) {
			this.mGpsTracker.stopLocationListener();
		}
		this.finish();
		Animation.setTransitionLeftRight(this);
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == MapActivity.GPS_SETTINGS) {
			if (this.mGpsTracker.isProviderEnabled()) {
				this.mGpsTracker.setLocationManager();
			} else {
				this.showSettingsAlert();
			}
		} else if (requestCode == MapActivity.REQUEST_INFO) {

		}
	}

	private void loadData() {
		TravelManager.getInstance().setIRouteListener(this);
		TravelManager.getInstance().setContext(this);

		if (this.getIntent().hasExtra(MapActivity.GPS)) {
			TravelManager.getInstance().setGps(true);
			this.mGpsTracker = new GpsTracker(this, this);
		} else {
			TravelManager.getInstance().setGps(false);
		}
	}

	private void initComponents() {
		this.setContentView(R.layout.activity_map);

		((TextView) this.findViewById(R.id.topBarTitle)).setText("Mapa");

		try {
			this.initGoogleMap();
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	private void initGoogleMap() {
		if (this.mGoogleMapFragment == null) {
			this.mGoogleMapFragment = ((SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.map));
			if (this.mGoogleMapFragment == null) {
				ToastUtils.showQuickCenteredToast(this, "Ocorreu um problema ao criar o mapa.");
			} else {

				this.mGoogleMapFragment.getMap().getUiSettings().setZoomControlsEnabled(false);

				this.mZoom = 3;
				final CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(-14.6868738, -51.884115)).zoom(this.mZoom).build();
				this.mGoogleMapFragment.getMap().animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

				this.mGoogleMapFragment.getMap().setOnCameraChangeListener(new OnCameraChangeListener() {
					@Override
					public void onCameraChange(final CameraPosition pCameraPosition) {
						MapActivity.this.mZoom = pCameraPosition.zoom;
					}
				});

				this.mGoogleMapFragment.getMap().setOnMapLongClickListener(this);
			}
		}
	}

	private void showSettingsAlert() {
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("GPS");
		alertDialog.setMessage("GPS n�o est� habilitado. Deseja ir �s configura��es?");
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				MapActivity.this.startActivityForResult(intent, MapActivity.GPS_SETTINGS);
			}
		});

		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.cancel();
				MapActivity.this.finish();
				Animation.setTransitionLeftRight(MapActivity.this);
			}
		});
		alertDialog.show();
	}

	private void showMapClickAlert(final LatLng pLatLng) {
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Ponto");
		// alertDialog.setMessage(
		// "Deseja definir esta posi��o como qual ponto?" );

		final String options[] = { "In�cio", "Fim", "Cancelar" };
		alertDialog.setItems(options, new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				if (which == 0) {
					TravelManager.getInstance().setStartLocation(pLatLng);
					MapActivity.this.mGoogleMapFragment.getMap().addMarker(MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TYPE_GREEN_LITTLE).position(pLatLng));
					MapActivity.this.findViewById(R.id.viewPositionStart).setBackgroundColor(Color.WHITE);
				} else if (which == 1) {
					TravelManager.getInstance().setEndLocation(pLatLng);
					MapActivity.this.mGoogleMapFragment.getMap().addMarker(MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TYPE_RED_LITTLE).position(pLatLng));
					MapActivity.this.findViewById(R.id.viewPositionEnd).setBackgroundColor(Color.WHITE);
				}
				dialog.cancel();
			}
		});
		alertDialog.show();
	}

	@Override
	public void onLocationUpdate(final double pLatitude, final double pLongitude) {
		if (this.mMarker == null) {
			this.mMarker = MarkerOptionsFactory.get(MarkerOptionsFactory.MARKER_TYPE_CAR_LITTLE).position(new LatLng(pLatitude, pLongitude)).title("Voc� est� aqui.");
			this.mGoogleMapFragment.getMap().addMarker(this.mMarker);
		} else {
			this.mMarker.position(new LatLng(pLatitude, pLongitude));
		}

		final CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(pLatitude, pLongitude)).zoom(this.mZoom).build();
		this.mGoogleMapFragment.getMap().animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	@Override
	public void onSettingsDisabled() {
		this.showSettingsAlert();
	}

	@Override
	public void onMapLongClick(final LatLng pLatLng) {
		this.showMapClickAlert(pLatLng);
	}

	public void onClickTracarRota(final View pView) {
		if (Connectivity.checkConnectivity(this) != Connectivity.NO_CONNECTION_AVAILABLE) {
			if (TravelManager.getInstance().isLocationsSetted()) {
				TravelManager.getInstance().calculateRoute();
				(this.findViewById(R.id.layoutInfo)).setVisibility(View.VISIBLE);
			} else {
				ToastUtils.showCenteredToast(this, "Selecione os pontos de inicio e fim! Basta clicar e segurar em algum ponto do mapa.");
			}
		} else {
			ToastUtils.showCenteredToast(this, "Conecte-se � internet.");
		}
	}

	public void onClickPositions(View pView) {
		ToastUtils.showCenteredToast(this, "Para selecionar os pontos de in�cio e fim, toque e segure no ponto desejado no mapa.");
	}

	public void onClickInfo(View pView) {
		this.startActivityForResult(new Intent(this, TravelActivity.class), MapActivity.REQUEST_INFO);
		Animation.setTransitionBottonUp(this);
	}

	public void onClickMenu(final View pView) {
		this.openOptionsMenu();
	}

	@Override
	public GoogleMap getGoogleMap() {
		return this.mGoogleMapFragment.getMap();
	}

	@Override
	public Context getContext() {
		return this;
	}
}
