package br.com.pedagioon.pedagioon2.activities;

import java.math.BigDecimal;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import br.com.mlweb.libs.animation.Animation;
import br.com.mlweb.libs.messages.ToastUtils;
import br.com.mlweb.libs.text.Formatter;
import br.com.pedagioon.pedagioon2.R;
import br.com.pedagioon.pedagioon2.managers.DataManager;

public class CarActivity extends Activity {

	private EditText	mEditTextConsumption;
	private EditText	mEditTextFuelPrice;
	private EditText	mEditTextAxles;

	private float		mConsumption;
	private float		mFuelPrice;
	private int			mAxlesNumber;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.loadData();
		this.initComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.getMenuInflater().inflate(R.menu.car, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_exit:
			this.finish();
			Animation.setTransitionTopDownOnlyCurrentActivity(this);
			break;

		case R.id.action_save:
			this.getValues();

			DataManager.getInstance().setAxlesNumber(this.mAxlesNumber);
			DataManager.getInstance().setConsumption(this.mConsumption);
			DataManager.getInstance().setFuelPrice(this.mFuelPrice);

			DataManager.getInstance().saveData(this);

			ToastUtils.showCenteredToast(this, "As configura��es foram salvas.");
			break;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onBackPressed() {
		this.finish();
		Animation.setTransitionTopDownOnlyCurrentActivity(this);
	}

	private void loadData() {
		DataManager.getInstance().loadData(this);

		this.mFuelPrice = DataManager.getInstance().getFuelPrice();
		this.mConsumption = DataManager.getInstance().getConsumption();
		this.mAxlesNumber = DataManager.getInstance().getAxlesNumber();
	}

	private void initComponents() {
		this.setContentView(R.layout.activity_car);

		((TextView) this.findViewById(R.id.topBarTitle)).setText("Ve�culo");

		this.mEditTextAxles = (EditText) this.findViewById(R.id.editTextInputAxle);
		this.mEditTextConsumption = (EditText) this.findViewById(R.id.editTextInputConsumption);
		this.mEditTextFuelPrice = (EditText) this.findViewById(R.id.editTextInputFuelPrice);

		this.mEditTextAxles.setText(String.valueOf(this.mAxlesNumber));
		this.mEditTextFuelPrice.setText(Formatter.simpleCurrencyFormat(this.mFuelPrice, Locale.getDefault()));
		this.mEditTextConsumption.setText(Formatter.simpleDoubleDigitsFormat(this.mConsumption, 1, 1));

		this.mEditTextFuelPrice.addTextChangedListener(new TextWatcher() {
			String	current	= "";

			@Override
			public void afterTextChanged(final Editable s) {
			}

			@Override
			public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
			}

			@Override
			public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
				if (!s.toString().equals(this.current)) {
					CarActivity.this.mEditTextFuelPrice.removeTextChangedListener(this);

					final String cleanString = s.toString().replaceAll("[R$,.]", "");
					final BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
					CarActivity.this.mFuelPrice = parsed.floatValue();
					final String formated = Formatter.simpleCurrencyFormat(CarActivity.this.mFuelPrice, Locale.getDefault());
					this.current = formated;
					CarActivity.this.mEditTextFuelPrice.setText(formated);
					CarActivity.this.mEditTextFuelPrice.setSelection(formated.length());
					CarActivity.this.mEditTextFuelPrice.addTextChangedListener(this);
				}
			}
		});
	}

	private void getValues() {
		this.mConsumption = Float.parseFloat(this.mEditTextConsumption.getText().toString());
		this.mAxlesNumber = Integer.parseInt(this.mEditTextAxles.getText().toString());
	}

	public void onClickSalvar(final View pView) {
		this.getValues();

		DataManager.getInstance().setAxlesNumber(this.mAxlesNumber);
		DataManager.getInstance().setConsumption(this.mConsumption);
		DataManager.getInstance().setFuelPrice(this.mFuelPrice);

		DataManager.getInstance().saveData(this);

		this.finish();
		Animation.setTransitionTopDownOnlyCurrentActivity(this);
	}

	public void onClickMenu(final View pView) {
		this.openOptionsMenu();
	}

}
