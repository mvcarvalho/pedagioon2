package br.com.pedagioon.pedagioon2.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import br.com.mlweb.libs.animation.Animation;
import br.com.pedagioon.pedagioon2.R;

public class SplashScreenActivity extends Activity {
	
	@Override
	protected void onCreate( final Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		this.setContentView( R.layout.activity_splash_screen );
		
		new Handler().postDelayed( new Runnable() {
			@Override
			public void run() {
				SplashScreenActivity.this.startActivity( new Intent( SplashScreenActivity.this, MenuActivity.class ) );
				SplashScreenActivity.this.finish();
				Animation.setTransitionRightLeft( SplashScreenActivity.this );
			}
		}, 1000 );
	}
}
