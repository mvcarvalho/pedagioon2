package br.com.pedagioon.pedagioon2.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.mlweb.libs.animation.Animation;
import br.com.mlweb.libs.messages.ToastUtils;
import br.com.mlweb.libs.network.Connectivity;
import br.com.pedagioon.pedagioon2.R;
import br.com.pedagioon.pedagioon2.managers.DataManager;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.loadData();
		this.initComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_exit:
			this.finish();
			break;

		case R.id.action_travel:
			if (Connectivity.checkConnectivity(this) != Connectivity.NO_CONNECTION_AVAILABLE) {
				final Intent intent = new Intent(this, MapActivity.class);
				intent.putExtra(MapActivity.NEW_TRAVEL, true);
				this.startActivity(intent);
				Animation.setTransitionRightLeft(this);

			} else {
				ToastUtils.showCenteredToast(this, "Voc� precisa estar conectado � internet!");
			}
			break;

		case R.id.action_gps:
			if (Connectivity.checkConnectivity(this) != Connectivity.NO_CONNECTION_AVAILABLE) {
				final Intent intent = new Intent(this, MapActivity.class);
				intent.putExtra(MapActivity.GPS, true);
				this.startActivity(intent);
				Animation.setTransitionRightLeft(this);

			} else {
				ToastUtils.showCenteredToast(this, "Voc� precisa estar conectado � internet!");
			}
			break;

		case R.id.action_settings:
			final Intent intent = new Intent(this, CarActivity.class);
			this.startActivity(intent);
			Animation.setTransitionBottonUpOnlyNewActivity(this);
			break;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void loadData() {
		DataManager.getInstance().loadData(this);
	}

	private void initComponents() {
		this.setContentView(R.layout.activity_menu);
		((TextView) this.findViewById(R.id.topBarTitle)).setText("Pedagio ON!");
	}

	public void onClickNewTravel(final View pView) {
		if (Connectivity.checkConnectivity(this) != Connectivity.NO_CONNECTION_AVAILABLE) {
			final Intent intent = new Intent(this, MapActivity.class);
			intent.putExtra(MapActivity.NEW_TRAVEL, true);
			this.startActivity(intent);
			Animation.setTransitionRightLeft(this);

		} else {
			ToastUtils.showCenteredToast(this, "Voc� precisa estar conectado � internet!");
		}
	}

	public void onClickSetupCar(final View pView) {
		final Intent intent = new Intent(this, CarActivity.class);
		this.startActivity(intent);
		Animation.setTransitionBottonUpOnlyNewActivity(this);
	}

	public void onClickGps(final View pView) {
		if (Connectivity.checkConnectivity(this) != Connectivity.NO_CONNECTION_AVAILABLE) {
			final Intent intent = new Intent(this, MapActivity.class);
			intent.putExtra(MapActivity.GPS, true);
			this.startActivity(intent);
			Animation.setTransitionRightLeft(this);

		} else {
			ToastUtils.showCenteredToast(this, "Voc� precisa estar conectado � internet!");
		}
	}

	public void onClickMenu(final View pView) {
		this.openOptionsMenu();
	}

	public void onClickExit(final View pView) {
		this.finish();
	}
}
