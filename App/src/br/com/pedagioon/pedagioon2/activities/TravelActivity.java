package br.com.pedagioon.pedagioon2.activities;

import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.mlweb.libs.animation.Animation;
import br.com.mlweb.libs.text.Formatter;
import br.com.pedagioon.pedagioon2.R;
import br.com.pedagioon.pedagioon2.managers.DataManager;
import br.com.pedagioon.pedagioon2.managers.TravelManager;
import br.com.pedagioon.pedagioon2.vos.TollVO;

public class TravelActivity extends Activity {

	private String	valorTotal;
	private String	valorCombustivel;
	private String	distanciaTotal;
	private String	valorPedagio;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.loadData();
		this.initComponents();
	}

	@Override
	public void onBackPressed() {
		this.finish();
		Animation.setTransitionTopDownOnlyCurrentActivity(this);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.getMenuInflater().inflate(R.menu.travel, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_back_to_map:
			this.finish();
			Animation.setTransitionTopDownOnlyCurrentActivity(this);
			break;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void loadData() {
		double distance = TravelManager.getInstance().getRoute().getDistance();
		double fuelConsumption = distance / DataManager.getInstance().getConsumption();
		double fuelCost = fuelConsumption * DataManager.getInstance().getFuelPrice();
		double tollCost = 0.0;
		int axleNumber = DataManager.getInstance().getAxlesNumber();

		int tollsNumber = 0;
		for (TollVO tollVO : TravelManager.getInstance().getRoute().getTolls()) {
			if (axleNumber == 2) {
				tollCost += tollVO.getPricecar();
			} else {
				tollCost += (tollVO.getPriceaxle() / 2) * axleNumber;
			}
			tollsNumber++;
		}

		double totalCost = fuelCost + tollCost;

		this.valorTotal = this.formatarValor(totalCost, totalCost * 2);
		this.valorCombustivel = this.formatarValor(fuelCost, fuelCost * 2);
		this.valorPedagio = this.formatarValorPedagio(tollCost, tollCost * 2, tollsNumber);
		this.distanciaTotal = this.formatarDistancia(distance, distance * 2);
	}

	private void initComponents() {
		this.setContentView(R.layout.activity_travel);

		((TextView) this.findViewById(R.id.topBarTitle)).setText("Mapa");
		((TextView) this.findViewById(R.id.textViewTotalCost)).setText(this.valorTotal);
		((TextView) this.findViewById(R.id.textViewTotalFuel)).setText(this.valorCombustivel);
		((TextView) this.findViewById(R.id.textViewTotalTolls)).setText(this.valorPedagio);
		((TextView) this.findViewById(R.id.textViewTotalDistance)).setText(this.distanciaTotal);
	}

	public void onClickGoMap(final View pView) {
		this.finish();
	}

	private String formatarValor(double value1, double value2) {
		return Formatter.simpleCurrencyFormat(value1, Locale.getDefault()) + " / " + Formatter.simpleCurrencyFormat(value2, Locale.getDefault());
	}

	private String formatarDistancia(double value1, double value2) {
		return Formatter.simpleDoubleDigitsFormat(value1, 2, 1) + "km / " + Formatter.simpleDoubleDigitsFormat(value2, 2, 1) + "km";
	}

	private String formatarValorPedagio(double value1, double value2, int numPedagios) {
		return numPedagios + "-" + Formatter.simpleCurrencyFormat(value1, Locale.getDefault()) + " / " + (numPedagios * 2) + "-" + Formatter.simpleCurrencyFormat(value2, Locale.getDefault());
	}

	public void onClickMenu(final View pView) {
		this.openOptionsMenu();
	}
}
