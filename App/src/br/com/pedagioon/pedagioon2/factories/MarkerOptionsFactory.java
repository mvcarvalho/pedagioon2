package br.com.pedagioon.pedagioon2.factories;

import br.com.pedagioon.pedagioon2.R;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Fonte: http://stackoverflow.com/a/14812104/575643
 */
public class MarkerOptionsFactory {
	public static final int		MARKER_STAR					= R.drawable.toten_estrela;
	public static final int		MARKER_TOLL					= R.drawable.marker_toll_small;
	public static final int		MARKER_TYPE_GREEN			= R.drawable.marker_start;
	public static final int		MARKER_TYPE_GREEN_LITTLE	= R.drawable.marker_start_dest;
	public static final int		MARKER_TYPE_RED				= R.drawable.marker;
	public static final int		MARKER_TYPE_RED_LITTLE		= R.drawable.marker_end;
	public static final int		MARKER_TYPE_CAR_LITTLE		= R.drawable.marker_car;

	private static final float	PIN_MARKER_X_ANCHOR_DISP	= 0.28838951310861f;
	private static final float	PIN_MARKER_Y_ANCHOR_DISP	= 0.95238095238095f;
	private static final float	STAR_X_DISP					= 0.35294117647059f;
	private static final float	STAR_Y_DISP					= 0.82905982905983f;

	public static MarkerOptions get(final int resId) {
		final MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.icon(BitmapDescriptorFactory.fromResource(resId));

		switch (resId) {
		case MARKER_TYPE_RED:
		case MARKER_TYPE_GREEN:
		case MARKER_TYPE_GREEN_LITTLE:
		case MARKER_TYPE_RED_LITTLE:
		case MARKER_TYPE_CAR_LITTLE:
			MarkerOptionsFactory.pinMarker(markerOptions);
			break;

		case MARKER_TOLL:
			MarkerOptionsFactory.tollMarker(markerOptions);
			break;

		case MARKER_STAR:
			MarkerOptionsFactory.starMarker(markerOptions);
			break;
		}

		return markerOptions;
	}

	private static void pinMarker(final MarkerOptions markerOptions) {
		markerOptions.anchor(MarkerOptionsFactory.PIN_MARKER_X_ANCHOR_DISP, MarkerOptionsFactory.PIN_MARKER_Y_ANCHOR_DISP);
	}

	private static void starMarker(final MarkerOptions markerOptions) {
		markerOptions.anchor(MarkerOptionsFactory.STAR_X_DISP, MarkerOptionsFactory.STAR_Y_DISP);
	}

	private static void tollMarker(final MarkerOptions markerOptions) {
		markerOptions.anchor(MarkerOptionsFactory.PIN_MARKER_X_ANCHOR_DISP, MarkerOptionsFactory.PIN_MARKER_Y_ANCHOR_DISP);
	}
}
